# How can you setup:

Install all required tools [here](readme/prepare-your-environment.md)

Setup everything:

- [Using stow](#using-stow) ❗️❗️❗️ (more `automated`)
- Step-by-step following [my instructions](#step-by-step) (`manual, but structured`)

# Using Stow
[Stow](https://www.gnu.org/software/stow/) is a great tool to store .dotfiles (such as .zsh in current point) into one folder, 
making history of their changes, and simply setup / unsetup configs during simple commands.

## Setup
1. Clone repo into some custom dir

        git clone git@gitlab.com:tuus-amicus/z.sh-config.git <custom_dir_name>
2. Install stow 
3. Setup configs:
    - `stow -vtn <where_to_stow> <what_to_stow>` -> list what and where will be links created. 
    - `stow -St <where_to_stow> <what_to_stow>` -> link everything
    - `stow -D <what_to_stow>` -> unlink everything
> I am using stow-local-gitignore to exclue unnecessary files from linking, you may change it as you wish
4. type zsh and follow instructions to install neccessary files OR open **.zshrc**, **.config/oh-my-zsh-custom** and configure plugins that you really need
5. Go to [After installation setup](#After-Installation-Complete)
6. HAVE FUN! 🤝

# Step-by-step
- [Setup zsh as default](#setup-zsh-as-default)
- [Install oh-my zsh](#install-oh-my-zsh)
- [Install font](#install-meslo-font)
- [Install autosuggestion](#install-auto-suggestions-for-oh-my-zsh)
- [Install syntax highlight](#install-zsh-syntax-highlight)
- Clone files and have fun! :v: 

## Setup zsh as default

First, check if zsh is listed as a valid shell by `cat /etc/shells`
If zsh is not listed, install it. For example, if you use apt or brew

    sudo apt-get install zsh 
    brew install zsh

Do, step 1 again and see the path of zsh. In my case, both `/usr/bin/zsh` and `/usr/zsh` is listed. If you like to compile and install the shell from scratch, be sure to add the path in `/etc/shells`.

Change the shell using

    chsh -s /usr/bin/zsh
From man page:

> -s, --shell SHELL The name of the user's new login shell. Setting this field to blank causes the system to select the default login shell.

Logout and login again.

## Install oh-my-zsh

Install with curl

    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

## After Installation Complete

After installation it's required to make extra steps to verify everything is working fine

### Install Meslo font

[click_me](https://github.com/powerline/fonts/blob/master/Meslo%20Slashed/Meslo%20LG%20S%20Regular%20for%20Powerline.ttf)

### Setup Iterm2

This step is optional, but required for correct visual effects in terminal 

1. Apply config `com.googlecode.iterm2.plist` (`optional`)
2. Upload `Darkula.json` profile (`required`)
