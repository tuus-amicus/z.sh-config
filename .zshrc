if [ "$(uname)" '==' "Darwin" ]; then
    osx="yes"
elif [ "$(expr substr $(uname -s) 1 5)" '==' "Linux" ]; then
    linux="yes"
fi

# Source env
source $HOME/.config/zsh/zsh-user/env.sh
# Custom envs
[[ -f "$HOME/.env.custom.sh" ]] && source $HOME/.env.custom.sh

# Source aliases
source $ZSH_PLUGINS_DIR/aliases.sh

# Source general custom functions
source $ZSH_PLUGINS_DIR/functions.sh

# Setup powerlevel10k or Starcast theme. More important for Warp terminal
source $ZSH_PLUGINS_DIR/select-theme.sh

# Source plugins in terminal
source $ZSH_PLUGINS_DIR/oh-my-zsh-plugins.sh

# Load oh-my-zsh.sh
source $ZSH/oh-my-zsh.sh

# Should be sourced after zsh configuration because of pip plugin
source $ZSH_PLUGINS_DIR/python-env.sh
 
# Setup AI in terminal
source $ZSH_PLUGINS_DIR/openai-integration/shell-gpt.sh


if [ -n "$osx" ];then
    # Include mysql path
    is_installed "mysql" && export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"

	# Check version of grep. We have to install GNU grep, 'cuz some scripts for FZF not working with BSD grep,
	# also there are some topics, that GNU GREP faster
	# Check if installed & set PATH to it, using grep instead of ggrep
	# Change to brew --prefix grep
	[ -d "/opt/homebrew/opt/grep/libexec/gnubin" ] && {
		PATH=/opt/homebrew/opt/grep/libexec/gnubin:$PATH
	} || {
		brew install grep
	}
fi

# Source marker helper with commands https://github.com/pindexis/marker
# source $ZSH_PLUGINS_DIR/marker-plugin/init.sh

# Install if not exist fd find replacement
source $ZSH_PLUGINS_DIR/fd/install.sh

# Source general-purpose command-line fuzzy finder
source $ZSH_PLUGINS_DIR/fzf/init.sh

# Install ag (silversearcher-ag) if not installed
is_installed "ag" || { 
    echo "ag is not installed, installing.."
    [[ -n "$osx" ]] && brew install the_silver_searcher || sudo apt-get install silversearcher-ag 
}

# Source kubectl aliases && fzf commands for one
[[ -n "$has_kuber" ]] && {
    #source $ZSH_PLUGINS_DIR/kubernetes/kubectl-aliases.sh
    ## May use another kubectl aliases (over 600) https://github.com/ahmetb/kubectl-aliases/tree/0533366d8e3e3b3987cc1b7b07a7e8fcfb69f93c
    source $ZSH_PLUGINS_DIR/kubernetes/.kubectl_aliases
    source $ZSH_PLUGINS_DIR/fzf/kube-fzf/init.sh
    ## also powerlevel10k sets kubectl visualizaitons
}

## z.sh
zcf diff-index --quiet HEAD -- || echo "You have unpushed changes in .zsh"
check_configs_relevance

unset -f check_configs_relevance
unset -f should_i_install

### END_FILE_INITIALIZATION 

# This MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/dterekhov/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/dterekhov/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/dterekhov/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/dterekhov/google-cloud-sdk/completion.zsh.inc'; fi

# Shell - GPT integration ZSH 
source $ZSH_PLUGINS_DIR/openai-integration/shell-gpt-shell-integration.sh