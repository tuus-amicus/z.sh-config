* [kubectx](https://github.com/ahmetb/kubectx) -> easily switch kube-context
* kube-shell -> as ipython interactive support for kuber
* kube-prompt analog of kube-shell, but uses go syntax instead of python