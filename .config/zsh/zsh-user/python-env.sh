# >> Maybe much better to use: python3 -m venv <MYVENV> 
# >> I would just avoid the use of virtualenv after Python3.3+ and instead use the standard shipped library venv
# 
# We should use alias in macOS system, 'cuz python2 is installed and required by OS 
type "python3" &>/dev/null && {
    alias python='python3' # We should use alias in macOS system, 'cuz python2 is installed and required by OS 
    export VIRTUALENVWRAPPER_PYTHON=$(which python3) # We should use alias in macOS system, 'cuz python2 is installed and required by OS 
    # export WORKON_HOME=$HOME/.virtualenvs # Using by default from plugin
}

# Python 3.10 has a higher priority than version 3
type "python3.10" &>/dev/null && {
    alias python='python3.10' 
    alias pip='python3.10 -m pip'
    alias pip3.9='python3 -m pip'
}

# More preferable to use instead of virtualwrapper
[[ -d "$HOME/.pyenv/" ]] && {
    export PYENV_ROOT="$HOME/.pyenv"
    # export PATH="$HOME/.pyenv/bin:$PATH"
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    # eval "$(pyenv virtualenv-init -)"
}