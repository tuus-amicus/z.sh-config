[[ -d "$HOME/.marker" ]] || {
    git clone --depth=1 https://github.com/pindexis/marker $HOME/.marker && $HOME/.marker/install.py
}

# <C-spc> marker show
export MARKER_KEY_MARK='\ek'
export MARKER_KEY_NEXT_PLACEHOLDER='\et'
source $HOME/.local/share/marker/marker.sh
