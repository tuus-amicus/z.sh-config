# Docker
# -------------

## Select a docker container to start and attach to
function datch() {
  is_installed "docker" || return 1

  local cid
  cid=$(docker ps -a | sed 1d | fzf -1 -q "$1" | awk '{print $1}')

  [ -n "$cid" ] && docker start "$cid" && docker attach "$cid"
}
# Select a running docker container to stop
function dstp() {
  type docker &>/dev/null || {echo "docker not installed"; return}
	
  local cid
  cid=$(docker ps | sed 1d | fzf -q "$1" | awk '{print $1}')

  [ -n "$cid" ] && docker stop "$cid"
}
