#!/bin/bash

[ -n "$osx" ] && script_root=$(cd `dirname $0` && pwd)

[ -n "$linux" ] && script_root=$(dirname $(readlink -f $0))

# Check if it's installed
[[ -f $HOME/.fzf.zsh ]] || . $script_root/install.sh

. $HOME/.fzf.zsh
. $script_root/env.sh
. $script_root/functions.sh
. $script_root/key-bindings.zsh
