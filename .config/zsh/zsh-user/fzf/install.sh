#!/bin/bash

echo "fzf not installed. Installing.."
if [[ -n "$linux" ]]; then
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install && echo "Fzf installed"
fi

if [[ -n "$osx" ]]; then
    brew install fzf
    $(brew --prefix)/opt/fzf/install && echo "Fzf installed"
fi
