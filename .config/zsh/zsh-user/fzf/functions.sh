. "$script_root/git-functions.sh"
. "$script_root/docker-functions.sh"
#. "$script_root/marker-functions.sh"

# --------------------------------------------------
# Changing directory
# --------------------------------------------------

# fzpar - cd to selected parent directory
fzpar() {
	is_installed "realpath" || returna
	local declare dirs=()
	get_parent_dirs() {
		if [[ -d "${1}" ]]; then dirs+=("$1"); else return; fi
		if [[ "${1}" == '/' ]]; then
			for _dir in "${dirs[@]}"; do echo $_dir; done
		else
			get_parent_dirs $(dirname "$1")
		fi
	}
	local DIR=$(get_parent_dirs $(realpath "${1:-$PWD}") | fzf-tmux --tac)
	cd "$DIR"
}

# fzgcd - fuzzy cd from anywhere (global) file
# ex: fzcd word1 word2 ... (even part of a file name)
# zsh autoload function
fzgcd() {
  local file
  [ -n "$osx" ] && opt="-i" || opt="-Ai"
  
  file="$(locate $opt -0 $@ | grep -z -vE '~$' | fzf --read0 -0 -1)"

  if [[ -n $file ]]
  then
     if [[ -d $file ]]
     then
        cd -- $file
     else
        cd -- ${file:h}
     fi
  fi
}

# fzlcd(fuzzy local file cd) - cd into the directory of the selected file
fzlcd() {
   local file
   local dir
   file=$(fzf +m -q "$1") && dir=$(dirname "$file") && cd "$dir"
}

# Like normal cd but opens an interactive navigation window when called with no arguments. 
# For ls, use -FG instead of --color=always on osx.
[ -n "$osx" ] && lsOpt="-FG"
[ -n "$linux" ] && lsOpt="--color=always"

function cd() {
    if [[ "$#" != 0 ]]; then
        builtin cd "$@";
        return
    fi
    while true; do
        local lsd=$(echo ".." && ls -p | grep '/$' | sed 's;/$;;')
        local dir="$(printf '%s\n' "${lsd[@]}" |
            fzf --reverse --preview '
                __cd_nxt="$(echo {})";
                __cd_path="$(echo $(pwd)/${__cd_nxt} | sed "s;//;/;")";
                echo $__cd_path;
                echo;
                ls -p $lsOpt "${__cd_path}";
        ')"
        [[ ${#dir} != 0 ]] || return 0
        builtin cd "$dir" &> /dev/null
    done
}

# --------------------------------------------------
# OPENING FILES
# --------------------------------------------------

# fzef(fuzzy emacs find) - fuzzy open with emacs from anywhere
# ex: fzef word1 word2 ... (even part of a file name)
# zsh autoload function

### LINUX ONLY, BECAUSE OF grep -z & locate -A GNU realization
# -A Print only entries that match all PATTERNs instead of requiring only one of them to match.
# -z Output a zero byte (the ASCII NUL character) instead of the character that normally follows  a  file  name.

fzef() {
  local files

  files=(${(f)"$(locate -Ai -0 $@ | grep -z -vE '~$' | fzf --read0 -0 -1 -m)"})

  if [[ -n $files ]]
  then
     subl $files
     print -l $files[1]
  fi
}

# fe [FUZZY PATTERN] - Open the selected file with the default editor
#   - Bypass fuzzy finder if there's only one match (--select-1)
#   - Exit if there's no match (--exit-0)
# fe() {
#   local files
#   IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
#   [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
# }

# # Modified version where you can press
# #   - CTRL-O to open with `open` command,
# #   - CTRL-E or Enter key to open with the $EDITOR
# fo() {
#   local out file key
#   IFS=$'\n' out=($(fzf-tmux --query="$1" --exit-0 --expect=ctrl-o,ctrl-e))
#   key=$(head -1 <<< "$out")
#   file=$(head -2 <<< "$out" | tail -1)
#   if [ -n "$file" ]; then
#     [ "$key" = ctrl-o ] && open "$file" || ${EDITOR:-vim} "$file"
#   fi
#}

# --------------------------------------------------
# SEARCHING CONTENT
# --------------------------------------------------
## fuzzy grep open via ag with line number
fzag() {
  is_installed "ag" || return

  local file
  local line

  read -r file line <<<"$(ag --nobreak --noheading $@ | fzf -0 -1 | awk -F: '{print $1, $2}')"

  if [[ -n $file ]]
  then
     subl $file:$line
  fi
}

fzgrep() {
  grep --line-buffered --color=never -r $# * | fzf
}

# CHROME
# Browsing Chrome history from fzf 
fzch() {
  local cols sep google_history open
  cols=$(( COLUMNS / 3 ))
  sep='{::}'

  # If sqlite3 not found, return
  type sqlite3 &>/dev/null || {echo "sqlite3 not installed"; return}
  
  if [ "$(uname)" = "Darwin" ]; then
    google_history="$HOME/Library/Application Support/Google/Chrome/Default/History"
    open=open
  else
    google_history="$HOME/.config/google-chrome/Default/History"
    open=xdg-open
  fi
  cp -f "$google_history" /tmp/h
  sqlite3 -separator $sep /tmp/h \
    "select substr(title, 1, $cols), url
     from urls order by last_visit_time desc" |
  awk -F $sep '{printf "%-'$cols's  \x1b[36m%s\x1b[m\n", $1, $2}' |
  fzf --ansi --multi | sed 's#.*\(https*://\)#\1#' | xargs $open > /dev/null 2> /dev/null
}

# bookmarks
# alternative: alias fzcba="$script_root/chrome-bookmarks.rb"
fzcb() {
	# Check linux or osx 
	[ -n "$osx" ] && $script_root/chrome-bookmarks.rb || $script_root/chrome-bookmarks-linux.rb
}

# -------------------------------------------

# ALT-I - Paste the selected entry from locate output into the command line
# Alt-i to paste item from locate / output (zsh only):
fzf-locate-widget() {
  local selected
  if selected=$(locate / | fzf -q "$LBUFFER"); then
    LBUFFER=$selected
  fi
  zle redisplay
}
zle     -N    fzf-locate-widget
bindkey '\ei' fzf-locate-widget

# -------------
# Processes
# -------------
# fkill - kill processes - list only the ones you can kill. Modified the earlier script.
fzkill() {
    local pid 
    if [ "$UID" != "0" ]; then
        pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
    else
        pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
    fi  

    if [ "x$pid" != "x" ]
    then
        echo $pid | xargs kill -${1:-9}
    fi  
}


# ------------
# Z support
# ------------

unalias z
z() {
  if [[ -z "$*" ]]; then
    cd "$(_z -l 2>&1 | fzf +s --tac | sed 's/^[0-9,.]* *//')"
  else
    _last_z_args="$@"
    _z "$@"
  fi
}

# Here is another version that also supports relaunching z with the arguments 
# for the previous command as the default input by using zz
zz() {
  cd "$(_z -l 2>&1 | sed 's/^[0-9,.]* *//' | fzf -q "$_last_z_args")"
}

# -------------
# Vagrant support
# -------------
# fuzzy vagrant ssh (fzvssh)
#List all vagrant boxes available in the system including its status, and try to access the selected one via ssh
fzvssh(){
	is_installed "jq" || return
	is_installed "vagrant" || return
	
	cd $(cat ~/.vagrant.d/data/machine-index/index | jq '.machines[] | {name, vagrantfile_path, state}' | jq '.name + "," + .state  + "," + .vagrantfile_path'| sed 's/^"\(.*\)"$/\1/'| column -s, -t | sort -rk 2 | fzf | awk '{print $3}'); vagrant ssh
}

# -------------
# Preview Files Before Selecting Them
# -------------
# https://pragmaticpineapple.com/four-useful-fzf-tricks-for-your-terminal/#4-preview-files-before-selecting-them

# You can achieve the similar using the power of the tree. I had to install it on macOS with brew install tree. Then, I added the _fzf_comprun function to load on shell initialization (you can add it in .bashrc or .zshrc). Here is how it looks:

_fzf_comprun() {
  local command=$1
  shift

  case "$command" in
    cd)           fzf "$@" --preview 'tree -C {} | head -200' ;;
    *)            fzf "$@" ;;
  esac
}
