# Bingings for git
join-lines() {
  local item
  while read item; do
    echo -n "${(q)item} "
  done
}

bind-git-helper() {
  local c
  for c in $@; do
	# Pattern $c -> f,b,t,r,h; evaluate fzf-[gf/gb/gt]-widget to bind for keys
    eval "fzf-g$c-widget() { local result=\$(fzg$c | join-lines); zle reset-prompt; LBUFFER+=\$result }"
    eval "zle -N fzf-g$c-widget"
    eval "bindkey '^g^$c' fzf-g$c-widget"
  done
}
bind-git-helper f b t r h
unset -f bind-git-helper

# Keybindings for marker
# zle -N _fzf_marker_main_widget
# zle -N _fzf_marker_placeholder_widget
# bindkey "${FZF_MARKER_MAIN_KEY:-\C-@}" _fzf_marker_main_widget
# bindkey "${FZF_MARKER_PLACEHOLDER_KEY:-\C-v}" _fzf_marker_placeholder_widget

##### Keymaps

# CD to directory
# ⌥ + c