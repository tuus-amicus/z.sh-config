# Alternatively, you might like to follow symbolic links and include hidden files (but exclude .git folders):
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git --color=always'

# Colorize (with --color=always)
export FZF_DEFAULT_OPTS="--ansi"

# Setup CTRL_T to default command (fd)
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
