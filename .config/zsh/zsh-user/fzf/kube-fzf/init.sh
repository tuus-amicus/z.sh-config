[[ $commands[findpod] ]] || {
    [[ -n "$osx" ]] && {
        brew tap thecasualcoder/stable
        brew install kube-fzf
    } || {
        [[ -d $HOME/.kube-fzf ]] || {
            git clone https://github.com/thecasualcoder/kube-fzf.git $HOME/.kube-fzf
            sudo ln -s ~/.kube-fzf/findpod /usr/local/bin/findpod
            sudo ln -s ~/.kube-fzf/execpod /usr/local/bin/execpod
            sudo ln -s ~/.kube-fzf/tailpod /usr/local/bin/tailpod
            sudo ln -s ~/.kube-fzf/describepod /usr/local/bin/describepod
            sudo ln -s ~/.kube-fzf/pfpod /usr/local/bin/pfpod
            sudo ln -s ~/.kube-fzf/kube-fzf.sh  /usr/local/bin/kube-fzf.sh
        } 
    }
}


