# Show full size human read of directories
alias dfs="du -hH -d=1"

# Aliases for zshconfig
alias zcf='/usr/bin/git --git-dir=$HOME/.dotfiles/zsh/.git/ --work-tree=$HOME/.dotfiles/zsh'
alias zcfs='zcf status'
alias zcfd='zcf diff'
alias zcfa='zcf add'
alias zcfl='zcf pull'
alias zcfp='zcf push'

# homebrew alias for cleaning
[[ -n "$osx" ]] &&  {
    alias brewclean='brew cleanup --prune-prefix; brew cleanup; brew doctor'
}

[[ -n "$linux" ]] && {
    # Add an "alert" alias for long running commands.  Use like so:
    #   sleep 10; alert
    alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

    # Alias definitions.
    # You may want to put all your additions into a separate file like
    # ~/.bash_aliases, instead of adding them here directly.
    # See /usr/share/doc/bash-doc/examples in the bash-doc package.
    if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
    fi

    # Workaround - ошибка SSL сертификата
    #export MAVEN_OPTS=-Dmaven.wagon.http.ssl.insecure=true
	
    # don't put duplicate lines or lines starting with space in the history.
    # See bash(1) for more options
    HISTCONTROL=ignoreboth

    # make less more friendly for non-text input files, see lesspipe(1)
    [ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
}
