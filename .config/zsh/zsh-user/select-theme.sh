#!/bin/zsh

## Starship theme for WrapTerminal
if [[ $TERM_PROGRAM == "WarpTerminal" ]]; then
    eval "$(starship init zsh)"
    # Warpify the output
    # printf '\eP$f{"hook": "SourcedRcFileForWarp", "value": { "shell": "zsh" }}\x9c' 
else
    # Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
    # Initialization code that may require console input (password prompts, [y/n]
    # confirmations, etc.) must go above this block; everything else may go below.
    if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
      source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
    fi
    source $ZSH_PLUGINS_DIR/powerlevel_k/setup.sh
fi