# Check if it's installed
type fd &>/dev/null || {
	echo "Fd not installed. Installing"
	[[ -n "$linux" ]] && {
		PKG=https://github.com/sharkdp/fd/releases/download/v7.3.0/fd-musl_7.3.0_amd64.deb
		echo "Installing fd version 7.3. Recommends to check newer version of sofware and change script installation"
		wget $PKG && sudo dpkg -i fd-musl_7.3.0_amd64.deb && rm fd-musl_7.3.0_amd64.deb
	} || {
		brew install fd
	}
}
