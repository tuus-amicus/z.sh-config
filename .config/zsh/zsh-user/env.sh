#!/bin/zsh

## TODO: refactor this -> load envs into one file

## Put these configs in .env.custom.sh and redefine values if need to enable

# Enable kuber-aliases
has_kuber=
# Enable jira plugin 
has_jira=

# jira environments for jira-plugin
[[ -f "$HOME/.jira-envs" ]] && source $HOME/.jira-envs

# Disable homebrew to autoupdate 'Updating Homebrew...'
[[ -n "$osx" ]] && export HOMEBREW_NO_AUTO_UPDATE=1

export PATH="$PATH:/usr/local/sbin"
# Setup directory where plugins store
export ZSH_PLUGINS_DIR=$HOME/.config/zsh/zsh-user
# Path to your oh-my-zsh installation
export ZSH="$HOME/.config/zsh/.oh-my-zsh"
# Path to custom ZSH dir, instead of .oh-my-zsh/custom
export ZSH_CUSTOM="$HOME/.config/zsh/.oh-my-zsh-custom"
