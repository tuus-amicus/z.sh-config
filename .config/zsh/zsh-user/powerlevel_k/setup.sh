## Setup powerlevel10k theme or download it
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
[[ -d $ZSH_CUSTOM/themes/powerlevel10k ]] || git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k

ZSH_THEME="powerlevel10k/powerlevel10k"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f $ZSH_PLUGINS_DIR/powerlevel_k/p10k.zsh ]] || {
  source $ZSH_PLUGINS_DIR/powerlevel_k/p10k.zsh
  # Unset described behavior in previous file
  typeset -g POWERLEVEL9K_KUBECONTEXT_SHOW_ON_COMMAND='kubectl|helm|kubens|kubectx|oc|istioctl|kogito|k9s|helmfile|fluxctl|stern|zkubectl'
}

#USE KUBE_PS1 my own
    # source $ZSH_PLUGINS_DIR/powerlevel_k/kube-ps1/setup.sh
# OR kubecontext PROMPT
#  # SEE p10k.zsh
    # POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS+=(kubecontext)  # current kubernetes context (https://kubernetes.io/)