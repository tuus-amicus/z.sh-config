#[[ -d ~/.oh-my-zsh/custom/plugins/kube-aliases ]] || git clone git@github.com:Dbz/kube-aliases.git ~/.oh-my-zsh/custom/plugins/

# Use common plugins (See bellow information about plugins)
plugins=(git
		 colorize
		 virtualenv
         # virtualenvwrapper
	     docker
		 python
		 zsh-syntax-highlighting
		 pip
		 zsh-autosuggestions
		 z
		 gradle)

[[ -n "$osx" ]] && plugins+=(brew macos)			

[[ -n "$has_jira" ]] && plugins+=(jira)
[[ -n "$has_kuber" ]] && plugins+=(kube-ps1 kubectl) # Enable kube-ps1 theme

[[ -d $ZSH_CUSTOM/plugins/autoupdate ]] \
    || git clone https://github.com/TamCore/autoupdate-oh-my-zsh-plugins $ZSH_CUSTOM/plugins/autoupdate \
    && plugins+=(autoupdate)

# https://github.com/Aloxaf/fzf-tab#oh-my-zsh
[[ -d $ZSH_CUSTOM/plugins/fzf-tab ]] \
	|| git clone https://github.com/Aloxaf/fzf-tab $ZSH_CUSTOM/plugins/fzf-tab \
	&& plugins+=(fzf-tab)

# Shell-gpt is using $ZSH_PLUGINS_DIR/openai-integration/shell-gpt.sh
# [[ -d $ZSH_CUSTOM/plugins/zsh_codex ]] \
# 	|| git clone https://github.com/tom-doerr/zsh_codex.git $ZSH_CUSTOM/plugins/zsh_codex \
# 	&& plugins+=(zsh_codex) \
# 	&& bindkey '^X' create_completion

### Fix slowness of pastes with zsh-syntax-highlighting.zsh
pasteinit() {
  OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
  zle -N self-insert url-quote-magic # I wonder if you'd need `.url-quote-magic`?
}

pastefinish() {
  zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish

# This plugin may slow your terminal's initalization
## plugins+=(kube-aliases)
## INSTEAD, USE aliases from kubernetes's dir 

#### NAME ######### DESCRIPTION ##########

# git           # Aliases for git

# colorize      # Plugin for highlighting file content

# virtualenv    # The plugin creates a virtualenv_prompt_info function that you can use in your theme, which displays the basename of the current $VIRTUAL_ENV. It uses two variables to control how that is shown: #ZSH_THEME_VIRTUALENV_PREFIX: sets the prefix of the VIRTUAL_ENV. Defaults to [.#ZSH_THEME_VIRTUALENV_SUFFIX: sets the suffix of the VIRTUAL_ENV. Defaults to ].

# python        # pyfind, pyclean, pygrep looks for text in .py https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/python

# zsh-syntax-highlighting # Download before git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# zsh-autosuggestions # auto suggestions

# brew          # aliases for brew

# osx           # tab, split_tab, pfs, cdf, quick-look, pushdf, showfiles, itunes, rmdsstore,  , etc

# github        # create remote repo, clone by name and repo without github prefix. INSTALL hub before setup & GitHub Gem ## DOESN'T work properly
# jira          # allow open url from terminal in JIRA. With brances, create new, reports, etc. Very very usefull.
# vagrant       # Uses vagrant aliases (Read what else)
# pip           # This plugin adds completion for pip (TAB), the Python package manager. https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/pip
# z             #  z - jump around
# gradle        # Gradle completion, see https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/gradle/gradle.plugin.zsh
# colored-man-pages # DOESN'T work properly somewhy
# docker        # https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/docker/README.md
# zsh_codex     # openai suggestions https://github.com/tom-doerr/zsh_codex # AI completion. Better to use sgpt https://github.com/TheR1D/shell_gpt
