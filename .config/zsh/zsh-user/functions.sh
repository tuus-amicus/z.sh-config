#!/bin/zsh

## Custom general functions

# File search functions
function f() { find . -iname "*$1*" ${@:2} }
function fin() { egrep -ir $2 "$1" * } # List containing of files
function finl() { egrep -ir $2 "$1" * } # List files contains something
function r() { grep "$1" ${@:2} -R . }

# Create a folder and move into it in one command
function mkcd() { mkdir -p "$@" && cd "$_"; }

function grepfind() { find . -type f -exce grep  -nH --null -e "$1" {} + }

# Find with ag $1 and replace on $2
function agreplace() { ag -0 -l "$1" | xargs -0 perl -pi.bak -e "s/$1/$2/g" }

# Get JSON from proxy api and parse it with jq
function update_curl_proxy() {
    JSON=$(curl http://pubproxy.com/api/proxy)
    # Using -r as raw-output to exctract without quotes
    IP=$(echo $JSON | jq '.data' | jq -r '.[0].ip');
    PORT=$(echo $JSON | jq '.data' | jq -r '.[0].port')

    export https_proxy=http://$IP:$PORT;
}

## zsh help-functions, that will be unset on the end of z.sh's init
# Common function to detect if program installed
is_installed() {
    type $1 &>/dev/null && return 0 || return 1
}

should_i_install() {
    echo "Seems, you haven't $1 installed. Do you wish to install this program?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) return 0;;
            No ) return 1;;
        esac
    done
}

# Git functions to have repo with relevance configs
check_configs_relevance() { 
    # Checks if one of configs needed to be pushed

    ## sublime
    if [[ -n "$linux" ]]; then 
        SUBLIME_DIR="$HOME/.config/sublime-text-3/Packages/User"
    else
        SUBLIME_DIR="$HOME/Library/Application Support/Sublime Text/Packages/User"
    fi

    [ -d $SUBLIME_DIR ] && {
        git -C $SUBLIME_DIR diff-index --quiet HEAD -- || echo "You have unpushed changes in sublime"
    }

    ## emacs.d
    # git -C $HOME/.emacs.d diff-index --quiet HEAD -- || echo "You have unpushed changes in .emacs.d"
}